using GraphRecipes, Plots


###############################################################################
#                                                                             #
#               Defining the functions and their possible actions             #
#                                                                             #
###############################################################################

domain_max = 10

println("Enter: the number of functions/variables")
num_variables = readline()

function def_function(num_variables)
    
    variables = Dict(

    )

    for i in 1:parse(Int64, num_variables)
        println("Enter the functions ID")
        id = readline()
        println("Enter the minumum response time")
        min_responseTime = parse(Int64, readline())

        println("Enter the maximun latency")
        max_latency = parse(Int64, readline())

        println("Enter the minimum throughput")
        min_throughput = parse(Int64, readline())

        println("Enter the minimum success rate")
        min_successRate = parse(Int64, readline())

        merge!(variables, Dict(parse(Int64, "$i") => (id, [min_responseTime:domain_max],[1:max_latency],[min_throughput:domain_max],[min_successRate:domain_max])))
    end

    return(variables)
end

variables = def_function(num_variables)

println(variables)


###############################################################################
#                                                                             #
#                   Defining constraints for the functions                    #
#                                                                             #
###############################################################################

println("How many constraints are there for the functions?")
num_constraints = parse(Int64, readline())

function set_constraints(num_constraints)
    constraints = []

    for i in 1:num_constraints
        println("What funcions are you setting constraints for? Enter them separed by a space")
        funct_for_const = split(readline(), " ")

        println("Choose a constraints\nEnter only the number\n1.Not equals\n2.Equals\n3.Greater than\n4.Less than")
        answer = parse(Int64, readline())
        first = funct_for_const[1]
        sec = funct_for_const[2]
        if answer == 1
            push!(constraints, "$first NE $sec")
        elseif answer == 2
            push!(constraints, "$first E $sec")
        elseif answer == 3
            push!(constraints, "$first GT $sec")
        elseif answer == 4
            push!(constraints, "$first LT $sec")
        end
    end
    return constraints
end 

constraints = set_constraints(num_constraints)

println(constraints)


###############################################################################
#                                                                             #
#                  Defining the arcs based on constraints                     #
#                                                                             #
###############################################################################

function def_arcs(constraints)
    arc = []
    n_split = []

    for i in 1:length(constraints)
        push!(n_split, split(constraints[i], " "))
        println(n_split)
    end
    for i in 1:length(n_split)
        #Removing Strings from the array
        for j in 1:length(n_split[i])
            if j % 2 == 0
                splice!(n_split[i], j)
            end
        end
    end
    
    for (n1, n2) in n_split
        push!(arc, [parse(Int64, n1), parse(Int64, n2)])
    end
    return arc
end
arc = def_arcs(constraints)
println(arc)


###############################################################################
#                                                                             #
#                      Drawing the Constraints Graph                          #
#                                                                             #
###############################################################################

function plot_constraints(variables, arc)
    nodes = variables
    arcs = arc

    graph = zeros(Int, length(nodes), length(nodes))
    for (n1, n2) in arcs 
        graph[n1, n2] = 1
    end
    names = 1:length(nodes)
    graphplot(graph, names=names, cureve=false, nodeshape=:circle,markercolor="blue", lineaplha=0.5)
end

# plot_constraints(variables, arc)


###############################################################################
#                                                                             #
#                   Checking constraints for the functions                    #
#                                                                             #
###############################################################################

function gt_constraints(x,y)
    #checks constraints that x is greater than y
    #This function adds all numeric values in the funtion for both functions, and returns true if the sum of x is greater than y
    n = 2
    costx = 0
    costy = 0
    for i in n:length(x)-1
        costx += x[n] + x[n+1]
    end
    for j in n:length(y)-1
        costy += y[n] + y[n+1]
    end

    if costx > costy return true else return false end
end


function lt_constraints(x,y)
    #checks constraints that x is greater than y
    #This function adds all numeric values in the funtion for both functions, and returns true if the sum of x is less than y
    n = 2
    costx = 0
    costy = 0
    for i in n:length(x)-1
        costx += x[n] + x[n+1]
    end
    for j in n:length(y)-1
        costy += y[n] + y[n+1]
    end

    if costx < costy return true else return false end
end

function eq_constraints(x,y)
    #checks constraints that x is equal to y
    #This function adds all numeric values in the funtion for both functions, and returns true if the sum of x is equal to y
    n = 2
    costx = 0
    costy = 0
    for i in n:length(x)-1
        costx += x[n] + x[n+1]
    end
    for j in n:length(y)-1
        costy += y[n] + y[n+1]
    end

    if costx == costy return true else return false end
end

function not_eq_constraints(x,y)
    #set constraints that x is not equal to y
    #This function adds all numeric values in the funtion for both functions, and returns true if the sum of x is not equal to y
    n = 2
    costx = 0
    costy = 0
    for i in n:length(x)-1
        costx += x[n] + x[n+1]
    end
    for j in n:length(y)-1
        costy += y[n] + y[n+1]
    end

    if costx != costy return true else return false end
end


###############################################################################
#                                                                             #
#                  Defining Forward checking & propagation                    #
#                                                                             #
###############################################################################

function forward_checking(constraints)
    supporCount = 0
    conflictingCount = 0
    ration = 0

    rationArray = []

    for i in 1:length(constraints)
        for j in 1:length(constraints[i])
            if constraints[i][2] == "NE" && not_eq_constraints(constraints[i][1], constraints[i][3]) == true
                supporCount += 1
            else 
                conflictingCount += 1
                variables[parse(Int64, constraints[i][1])] = variables[parse(Int64, constraints[i][3])]
            end
            if constraints[i][2] == "E" && eq_constraints(constraints[i][1], constraints[i][3]) == true
                supporCount += 1
            else
                conflictingCount += 1
                variables[parse(Int64, constraints[i][1])] += 1
            end
            if constraints[i][2] == "GT"
                supporCount += 1
            else
                conflictingCount += 1
            end
            if constraints[i][2] == "LT"
                supporCount += 1
            else 
                conflictingCount += 1
            end
            ration += conflictingCount/supporCount
        end
        push!(rationArray, ration)
    end
    return rationArray
end

plot_constraints(variables, arc)
