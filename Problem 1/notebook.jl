### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# ╔═╡ 3c55cd00-baac-11ec-0241-b563fe06353e
begin
	using Pluto
	using PlutoUI
end

# ╔═╡ 05557837-1839-428e-adb8-984295fbde95
md"# ARI711S Assignemt 2 Problem 1"

# ╔═╡ 2c114a14-5c70-48b0-9b2b-dfa73ee41c4b
md"The task is to write a program in Julia that implements a search-based
problem-solving mechanism using the A
∗ algorithm as a strategy. As a
heuristic function, you will apply 3 unit costs for each parcel not yet collected. A goal state is one where all parcels have been collected.
Your program will allow the user to provide a detailed configuration of the
problem at runtime. This includes the number of storeys, how many occupied offices per floor, the number of parcels in each office and the location
of the agent at the beginning.
"

# ╔═╡ 94449c67-374b-47ee-8bb5-810c8dc12350
numFloors = 2

# ╔═╡ 8b15c489-df97-4d02-b8d6-5656ea735305
floors = []

# ╔═╡ 8427b5ee-9299-4e82-8822-03d38dc43e84
Print("Enter below the number of offices per floor.")

# ╔═╡ cee522f3-9bf0-4667-a53f-7b6b4025994a
numOffices = 5

# ╔═╡ 38f1c7b9-b6a9-465b-9d73-a8c5a33b894d
currentFloor = 1

# ╔═╡ dc653171-fdf1-4153-b985-76cf46caef95
md"# Defining the Agent"

# ╔═╡ eedac3e7-61e8-4237-9bbb-dd87b229674e
mutable struct Agent 
	currentOffice::Int64
	currentFloor::Int64
	holdingParcel::Bool
end

# ╔═╡ 6368264c-4d9b-4146-bb20-3b1ca79210c9
md"# Defining the Agents Actions"

# ╔═╡ 29cf58e0-e990-4e31-8096-f1bdfe664d62
md"# Defining the State Space"

# ╔═╡ d9279c66-b33d-459a-9834-848b2a1751dc
struct Office
	officeNumber::Int64
	floor::Int64
	hasParcel::Bool
	parcelNumber::Int64
	parcelDestination::Int64
	west::Int64
	east::Int64
end

# ╔═╡ 0aed0adb-9d2f-4970-801c-cfdf90ab63a5
md"Defining each individual office"

# ╔═╡ 8cd7eb50-c804-41b3-a030-404102daefe0
begin
	f1_office1 = Office(1,1,true,1,5,0,2)
	f1_office2 = Office(2,1,true,2,5,1,3)
	f1_office3 = Office(3,1,true,3,5,2,4)
	f1_office4 = Office(4,1,true,4,5,3,0)
end

# ╔═╡ c4069601-b017-4738-8a79-d3b831dfafbf
agent = Agent(f1_office1.officeNumber, f1_office1.floor, false)

# ╔═╡ 56ab05cd-610a-46c5-8f73-b21a52a69942
begin
	function moveEast(y)
		#where y is current office
		#if not in office x, and office x exists, and current office is east of office, x move east
		currentOffice = y
		if agent.currentOffice < numOffices
			agent.currentOffice += 1
			return agent.currentOffice
		else return "Error office does not exist"
		end
	end
	function moveWest(y)
		#where y is current office
		#if not in office x, and office x exists, and current office is west of office, x move west
		currentOffice = y
		if agent.currentOffice > 1
			agent.currentOffice -= 1
			return agent.currentOffice
		else return "Error office does not exist"
		end
	end
	function moveUp(x)
		#where x is current floor
		#if not in floor y, and floor y exists, and floor y above current floor, move up.
		if agent.currentFloor < numFloors
			agent.curretnFloor += 1
			return agent.currentFloor
		else return "Error floor does not exist"
		end
	end
	function moveDown(x)
		#where x is current floor
		#if not in floor y, and floor y exists, and floor y above current floor, move up.
		if agent.currentFloor > 1
			agent.currentFloor -= 1
			return agent.currentFloor
		else return "Error floor does not exist"
		end
	end
	function collect(z)
		#where z is current office
		#if document exists, collect document
		if z.hasParcel == true
			z.hasParcel == false
		end
	end
end

# ╔═╡ 8d3b827c-fe83-45f6-b470-d9e6d5650e2f
begin
	f2_office1 = Office(21,2,true,21,25,0,22)
	f2_office2 = Office(22,2,true,22,25,21,23)
	f2_office3 = Office(23,2,true,23,25,22,24)
	f2_office4 = Office(24,2,true,24,25,23,25)
end

# ╔═╡ 32621ecd-efc2-4639-b54a-bc7957683c30
destination_1 = Office(5, 1, false,0,0,4,0)

# ╔═╡ 4d6597c5-9ad2-45b9-91fd-c0b501f8e7ce
destination_2 = Office(25, 2, false,0,0,24,0)

# ╔═╡ 1982bddb-a36b-46f9-b038-7b162aa1740c
collect(f2_office1)

# ╔═╡ 5892cbdb-a764-4f06-a207-d7faf923f447
moveEast(f2_office2.officeNumber)

# ╔═╡ 2843fbaa-1555-4c16-bf11-8bc2fbffc96c
moveWest(f2_office2.officeNumber)

# ╔═╡ 8045a4f7-0d7c-459e-842c-37534db90117
parcels = []

# ╔═╡ a7956c93-ee35-456e-b416-5aaf528581c8
begin 
	push!(parcels, f1_office1.parcelNumber)
	push!(parcels, f1_office2.parcelNumber)
	push!(parcels, f1_office3.parcelNumber)
	push!(parcels, f1_office4.parcelNumber)
	push!(parcels, f2_office1.parcelNumber)
	push!(parcels, f2_office2.parcelNumber)
	push!(parcels, f2_office3.parcelNumber)
	push!(parcels, f2_office4.parcelNumber)
end

# ╔═╡ e600f6d7-8389-4edc-b74a-3fb70148222b
offices = Dict(
	f1_office1.officeNumber => f1_office1, 
	f1_office2.officeNumber => f1_office2,
	f1_office3.officeNumber => f1_office3,
	f1_office4.officeNumber => f1_office4,
	5 => destination_1,
	f2_office1.officeNumber => f2_office1,
	f2_office2.officeNumber => f2_office2,
	f2_office3.officeNumber => f2_office3,
	f2_office4.officeNumber => f2_office4,
	25 => destination_2,
)

# ╔═╡ 3d199e4d-044e-441c-be3e-2e2a69e402bb
offices_mt = [
	offices[f1_office1.officeNumber] offices[f1_office2.officeNumber] offices[f1_office3.officeNumber] offices[f1_office4.officeNumber] destination_1; offices[f2_office1.officeNumber] offices[f2_office2.officeNumber] offices[f2_office3.officeNumber] offices[f2_office4.officeNumber] destination_2
]

# ╔═╡ 37ecfce7-3f95-433a-9964-5cb0977c721f
offices_mt

# ╔═╡ 91f3cb34-46ce-40ad-a2ee-b53b70de75f8
offices_mt[1, 3]

# ╔═╡ fd9bbcb1-b23e-4192-9e49-0739071ab9e0
offices_mt[agent.currentFloor, agent.currentOffice]

# ╔═╡ f478e3c1-c8b8-434b-b27e-e55acc74b808
md"# A* Algorithm" 

# ╔═╡ 703844b7-beb0-4f9e-9b7c-bdff404d5268
md"
f = g + h

where: \
f = least cost \
g = cost of moving from node to node\
h = heuristic cost\
"

# ╔═╡ 2a7bddd8-8f5f-4dee-8681-4bc5469a159a
md"#### Action costs

Where: \
move east: (me), to move to the next office eastward; \
move west: (mw), to move to the next office westward; \
move up: (mu), to move to the floor up; \
move down: (md), to move to the floor down; \
collect: (co), to collect a single item from an office."

# ╔═╡ 97e85ac8-a483-4661-adab-c0c112719a9e
begin
	me = 2
	mw = 2
	mu = 1
	md = 1
	co = 5
	
	function findNearest(office, goal)
		#f = g + h
		cost = 0
		
		if office >= goal 
			
			for i in goal:office
				moveWest(office)
				cost += mw
			end
			return cost
		else 
			for i in office:goal
				moveEast(office)
				cost += me
			end
			return cost
		end
	end
end

# ╔═╡ 59c0fe40-db60-47c7-8109-fbc8b8544681
findNearest(f1_office1.officeNumber, destination_1.officeNumber)

# ╔═╡ def5a780-0da1-45b1-8429-8b3c22a16245
moveWest(agent.currentOffice)

# ╔═╡ ba118741-0627-4f6d-b31b-53c95eea7694
moveEast(agent.currentOffice)

# ╔═╡ 2f5c5275-c624-4492-bd17-f5aeaf5e091d
md"# Automating collection"

# ╔═╡ 8dbc3f02-12d7-4fe1-ba3d-5f265a82d517
function collectAll()
	notClean = true
	
	for i in length(parcels)
		# Print("hello")
		# notClean = false

		#if agent in office x, and office x has parcel, collect parcel in office x, move to goal, drop parcel, find nearest, move to nearest, repeate. 
		
		if (offices[agent.currentOffice].hasParcel == true)
			collect(offices[agent.currentOffice])
			findNearest(agent.currentOffice, destination_1.officeNumber)
			pop!(parcels, offices[agent.currentOffice].parcelNumber)
		else
			if moveWest(agent) != 0
				moveWest(agent)
			else 
				moveEast(agent)
			end
		end
		
	end
	return "success"
end

# ╔═╡ 0576249e-bfbe-4164-8a06-e2706c7a284d
offices[agent.currentOffice].hasParcel

# ╔═╡ 10f4be6f-3c14-4394-9c2b-22dedc6c1731
moveWest(agent)

# ╔═╡ a4e15ebf-e283-4f49-a367-df09e57f091b
findNearest(agent.currentOffice, destination_1.officeNumber)

# ╔═╡ 50c68ca7-79f7-4e7f-8257-c26ff8526691
agent.currentOffice

# ╔═╡ fec65b6d-ea2d-4863-a277-8f38ab914138
offices[agent.currentOffice].hasParcel

# ╔═╡ a1540629-0721-4683-b50d-2e8707ea9641
# collect(offices[agent.currentOffice])

# ╔═╡ 16cfbd1f-6a3b-4f73-8836-64f9f1be8d99
collectAll()

# ╔═╡ 94df84c6-e80f-4490-a1db-1bb7dcdcce5b
offices_mt[]

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Pluto = "c3e4b0f8-55cb-11ea-2926-15256bba5781"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
Pluto = "~0.19.0"
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Configurations]]
deps = ["ExproniconLite", "OrderedCollections", "TOML"]
git-tree-sha1 = "ab9b7c51e8acdd20c769bccde050b5615921c533"
uuid = "5218b696-f38b-4ac9-8b61-a12ec717816d"
version = "0.17.3"

[[deps.DataAPI]]
git-tree-sha1 = "cc70b17275652eb47bc9e5f81635981f13cea5c8"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.9.0"

[[deps.DataValueInterfaces]]
git-tree-sha1 = "bfc1187b79289637fa0ef6d4436ebdfe6905cbd6"
uuid = "e2d170a0-9d28-54be-80f0-106bbe20a464"
version = "1.0.0"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.ExproniconLite]]
git-tree-sha1 = "8b08cc88844e4d01db5a2405a08e9178e19e479e"
uuid = "55351af7-c7e9-48d6-89ff-24e801d99491"
version = "0.6.13"

[[deps.FileWatching]]
uuid = "7b1f6079-737a-58dc-b8bc-7a2ca5c1b5ee"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.FuzzyCompletions]]
deps = ["REPL"]
git-tree-sha1 = "efd6c064e15e92fcce436977c825d2117bf8ce76"
uuid = "fb4132e2-a121-4a70-b8a1-d5b831dcdcc2"
version = "0.5.0"

[[deps.HTTP]]
deps = ["Base64", "Dates", "IniFile", "Logging", "MbedTLS", "NetworkOptions", "Sockets", "URIs"]
git-tree-sha1 = "0fa77022fe4b511826b39c894c90daf5fce3334a"
uuid = "cd3eb016-35fb-5094-929b-558a96fad6f3"
version = "0.9.17"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.IniFile]]
git-tree-sha1 = "f550e6e32074c939295eb5ea6de31849ac2c9625"
uuid = "83e8ac13-25f8-5344-8a64-a9f2b223428f"
version = "0.5.1"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.IteratorInterfaceExtensions]]
git-tree-sha1 = "a3f24677c21f5bbe9d2a714f95dcd58337fb2856"
uuid = "82899510-4779-5014-852e-03e436cf321d"
version = "1.0.0"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.MIMEs]]
git-tree-sha1 = "9be08aeb6aa3786bee586cdbfbbbc7e0b41b7bb2"
uuid = "6c6e2e6c-3030-632d-7369-2d6c69616d65"
version = "0.1.3"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS]]
deps = ["Dates", "MbedTLS_jll", "Random", "Sockets"]
git-tree-sha1 = "1c38e51c3d08ef2278062ebceade0e46cefc96fe"
uuid = "739be429-bea8-5141-9913-cc70e7f3736d"
version = "1.0.3"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.MsgPack]]
deps = ["Serialization"]
git-tree-sha1 = "a8cbf066b54d793b9a48c5daa5d586cf2b5bd43d"
uuid = "99f44e22-a591-53d1-9472-aa23ef4bd671"
version = "1.1.0"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "621f4f3b4977325b9128d5fae7a8b4829a0c2222"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.2.4"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.Pluto]]
deps = ["Base64", "Configurations", "Dates", "Distributed", "FileWatching", "FuzzyCompletions", "HTTP", "InteractiveUtils", "Logging", "MIMEs", "Markdown", "MsgPack", "Pkg", "REPL", "RelocatableFolders", "Sockets", "TOML", "Tables", "UUIDs"]
git-tree-sha1 = "8eeb55755dff8dc3c7acbee8989d27fda95fc737"
uuid = "c3e4b0f8-55cb-11ea-2926-15256bba5781"
version = "0.19.0"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.RelocatableFolders]]
deps = ["SHA", "Scratch"]
git-tree-sha1 = "307761d71804208c0c62abdbd0ea6822aa5bbefd"
uuid = "05181044-ff0b-4ac5-8273-598c1e38db00"
version = "0.2.0"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Scratch]]
deps = ["Dates"]
git-tree-sha1 = "0b4b7f1393cff97c33891da2a0bf69c6ed241fda"
uuid = "6c6a2e73-6563-6170-7368-637461726353"
version = "1.1.0"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.TableTraits]]
deps = ["IteratorInterfaceExtensions"]
git-tree-sha1 = "c06b2f539df1c6efa794486abfb6ed2022561a39"
uuid = "3783bdb8-4a98-5b6b-af9a-565f29a5fe9c"
version = "1.0.1"

[[deps.Tables]]
deps = ["DataAPI", "DataValueInterfaces", "IteratorInterfaceExtensions", "LinearAlgebra", "OrderedCollections", "TableTraits", "Test"]
git-tree-sha1 = "5ce79ce186cc678bbb5c5681ca3379d1ddae11a1"
uuid = "bd369af6-aec1-5ad0-b16a-f7cc5008161c"
version = "1.7.0"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.URIs]]
git-tree-sha1 = "97bbe755a53fe859669cd907f2d96aee8d2c1355"
uuid = "5c2747f8-b7ea-4ff2-ba2e-563bfd36b1d4"
version = "1.3.0"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═05557837-1839-428e-adb8-984295fbde95
# ╠═2c114a14-5c70-48b0-9b2b-dfa73ee41c4b
# ╠═3c55cd00-baac-11ec-0241-b563fe06353e
# ╠═94449c67-374b-47ee-8bb5-810c8dc12350
# ╠═8b15c489-df97-4d02-b8d6-5656ea735305
# ╠═8427b5ee-9299-4e82-8822-03d38dc43e84
# ╠═cee522f3-9bf0-4667-a53f-7b6b4025994a
# ╠═38f1c7b9-b6a9-465b-9d73-a8c5a33b894d
# ╠═dc653171-fdf1-4153-b985-76cf46caef95
# ╠═eedac3e7-61e8-4237-9bbb-dd87b229674e
# ╠═c4069601-b017-4738-8a79-d3b831dfafbf
# ╠═6368264c-4d9b-4146-bb20-3b1ca79210c9
# ╠═56ab05cd-610a-46c5-8f73-b21a52a69942
# ╠═29cf58e0-e990-4e31-8096-f1bdfe664d62
# ╠═d9279c66-b33d-459a-9834-848b2a1751dc
# ╠═0aed0adb-9d2f-4970-801c-cfdf90ab63a5
# ╠═8cd7eb50-c804-41b3-a030-404102daefe0
# ╠═8d3b827c-fe83-45f6-b470-d9e6d5650e2f
# ╠═32621ecd-efc2-4639-b54a-bc7957683c30
# ╠═4d6597c5-9ad2-45b9-91fd-c0b501f8e7ce
# ╠═1982bddb-a36b-46f9-b038-7b162aa1740c
# ╠═5892cbdb-a764-4f06-a207-d7faf923f447
# ╠═2843fbaa-1555-4c16-bf11-8bc2fbffc96c
# ╠═8045a4f7-0d7c-459e-842c-37534db90117
# ╟─a7956c93-ee35-456e-b416-5aaf528581c8
# ╠═e600f6d7-8389-4edc-b74a-3fb70148222b
# ╠═3d199e4d-044e-441c-be3e-2e2a69e402bb
# ╠═37ecfce7-3f95-433a-9964-5cb0977c721f
# ╠═91f3cb34-46ce-40ad-a2ee-b53b70de75f8
# ╠═fd9bbcb1-b23e-4192-9e49-0739071ab9e0
# ╠═f478e3c1-c8b8-434b-b27e-e55acc74b808
# ╠═703844b7-beb0-4f9e-9b7c-bdff404d5268
# ╠═2a7bddd8-8f5f-4dee-8681-4bc5469a159a
# ╠═97e85ac8-a483-4661-adab-c0c112719a9e
# ╠═59c0fe40-db60-47c7-8109-fbc8b8544681
# ╠═def5a780-0da1-45b1-8429-8b3c22a16245
# ╠═ba118741-0627-4f6d-b31b-53c95eea7694
# ╠═2f5c5275-c624-4492-bd17-f5aeaf5e091d
# ╠═8dbc3f02-12d7-4fe1-ba3d-5f265a82d517
# ╠═0576249e-bfbe-4164-8a06-e2706c7a284d
# ╠═10f4be6f-3c14-4394-9c2b-22dedc6c1731
# ╠═a4e15ebf-e283-4f49-a367-df09e57f091b
# ╠═50c68ca7-79f7-4e7f-8257-c26ff8526691
# ╠═fec65b6d-ea2d-4863-a277-8f38ab914138
# ╠═a1540629-0721-4683-b50d-2e8707ea9641
# ╠═16cfbd1f-6a3b-4f73-8836-64f9f1be8d99
# ╠═94df84c6-e80f-4490-a1db-1bb7dcdcce5b
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
