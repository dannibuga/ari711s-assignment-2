# ARI711S Assignment 2

By: Edilson Zau \
Student #: 220090491

## Problem 1

In this problem the task is to write a program in Julia that implements a search-based
problem-solving mechanism using the A∗ algorithm as a strategy. 

## Problem 2

In this problem, we wish to solve a function selection problem using constraint satisfaction techniques.
